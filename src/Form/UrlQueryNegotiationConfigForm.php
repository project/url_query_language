<?php

namespace Drupal\url_query_language\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\url_query_language\Plugin\LanguageNegotiation\LanguageNegotiationUrlQuery;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration form for the URL query language negotiation method.
 */
class UrlQueryNegotiationConfigForm extends ConfigFormBase {

  /**
   * The form ID.
   */
  const FORM_ID = 'url_query_language_negotiation_config';

  /**
   * The config name form reads and writes.
   */
  const CONFIG_NAME = LanguageNegotiationUrlQuery::CONFIG;

  /**
   * The config key of the query parameter name.
   */
  const QUERY_PARAMETER_SETTING = LanguageNegotiationUrlQuery::QUERY_PARAMETER_SETTING;

  /**
   * The config key of the query parameter overrides settings.
   */
  const QUERY_PARAMETER_OVERRIDES = LanguageNegotiationUrlQuery::QUERY_PARAMETER_OVERRIDES;

  /**
   * The route to redirect to after form submit.
   */
  const REDIRECT_ROUTE = 'language.negotiation';

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var static $instance */
    $instance = parent::create($container);

    $instance->languageManager = $container->get('language_manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return static::FORM_ID;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [static::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    global $base_url;
    $config = $this->config(static::CONFIG_NAME);

    $form[static::QUERY_PARAMETER_SETTING] = [
      '#required' => TRUE,
      '#type' => 'textfield',
      '#title' => $this->t('URL query parameter name'),
      '#description' => $this->t(
        'GET query parameter name to read the language from.'
      ),
      '#default_value' => $config->get(static::QUERY_PARAMETER_SETTING),
    ];

    $form['parameter_overrides'] = [
      '#type' => 'details',
      '#tree' => TRUE,
      '#title' => $this->t('Parameter overrides'),
      '#open' => TRUE,
      '#description' => $this->t('Language codes or other custom text to use as a query parameter value for URL language detection. For example, instead of "fr" for French, the site might require "french" for use with query parameters.<br/> <em>Leave values blank to fallback on the default Drupal language code.</em>'),
    ];
    $languages = $this->languageManager->getLanguages();
    $parameter_overrides = $config->get('parameter_overrides');
    foreach ($languages as $langcode => $language) {
      $t_args = ['%language' => $language->getName(), '%langcode' => $language->getId()];
      $form['parameter_overrides'][$langcode] = [
        '#type' => 'textfield',
        '#title' => $language->isDefault() ? $this->t('%language (%langcode) parameter value (Default language)', $t_args) : $this->t('%language (%langcode) parameter value', $t_args),
        '#maxlength' => 64,
        '#default_value' => $parameter_overrides[$langcode] ?? '',
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $parameter_key = static::QUERY_PARAMETER_SETTING;
    $parameter_overrides_key = static::QUERY_PARAMETER_OVERRIDES;

    $this->config(static::CONFIG_NAME)
      ->set($parameter_key, $form_state->getValue($parameter_key))
      ->set($parameter_overrides_key, $form_state->getValue($parameter_overrides_key))
      ->save();

    $form_state->setRedirect(static::REDIRECT_ROUTE);

    parent::submitForm($form, $form_state);
  }

}
