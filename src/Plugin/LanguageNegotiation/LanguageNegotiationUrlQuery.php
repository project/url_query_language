<?php

namespace Drupal\url_query_language\Plugin\LanguageNegotiation;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\language\LanguageNegotiationMethodBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class for identifying language via URL query parameter.
 *
 * @LanguageNegotiation(
 *   id = \Drupal\url_query_language\Plugin\LanguageNegotiation\LanguageNegotiationUrlQuery::METHOD_ID,
 *   weight = -9,
 *   name = @Translation("URL query parameter"),
 *   description = @Translation("Language from the GET query parameter."),
 *   config_route_name = "url_query_language.negotiation_config",
 * )
 */
class LanguageNegotiationUrlQuery extends LanguageNegotiationMethodBase implements OutboundPathProcessorInterface {

  /**
   * The plugin ID.
   */
  const METHOD_ID = 'url_query_language';

  /**
   * The config name form reads and writes.
   */
  const CONFIG = 'url_query_language.negotiation';

  /**
   * The config key of the query parameter name.
   */
  const QUERY_PARAMETER_SETTING = 'query_parameter';

  /**
   * The config key of the query parameter overrides settings.
   */
  const QUERY_PARAMETER_OVERRIDES = 'parameter_overrides';

  /**
   * Returns name of the URL query parameter to read the language code from.
   *
   * @return string
   *   The parameter name.
   */
  protected function getLangcodeQueryParameterName(): string {
    $config = $this->config->get(static::CONFIG);
    return $this->config->get(static::CONFIG)
      ->get(static::QUERY_PARAMETER_SETTING);
  }

  /**
   * Returns name of the URL query parameter to read the language code from.
   *
   * @param string $langcode
   *   The language code override value from the parameter match.
   *
   * @return string|null
   *   The matched internal Drupal language id or NULL if no match is found.
   */
  protected function getLangcodeQueryParameterOverride($langcode): string {
    $overrides = $this->config->get(static::CONFIG)
      ->get(static::QUERY_PARAMETER_OVERRIDES);
    $matched_langcode = array_search($langcode, $overrides, TRUE);
    // If no match was found, then return the passed langcode value.
    return $matched_langcode ? $matched_langcode : $langcode;
  }

  /**
   * {@inheritdoc}
   */
  public function getLangcode(Request $request = NULL) {
    if (!isset($request)) {
      return NULL;
    }

    $language = $this->getRequestQueryLanguage($request);
    if (!isset($language)) {
      return NULL;
    }

    return $language->getId();
  }

  /**
   * Returns the language found in the request query.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request to read the parameter from.
   * @param array|null $languages
   *   The list of available languages, indexed by language code. NULL means
   *   language manager is called for the list.
   *
   * @return \Drupal\Core\Language\LanguageInterface|null
   *   The language found in the request or NULL if nothing has been found.
   */
  protected function getRequestQueryLanguage(
    Request $request,
    array $languages = NULL
  ) {
    if (!$this->languageManager instanceof LanguageManagerInterface) {
      return NULL;
    }

    $parameter_name = $this->getLangcodeQueryParameterName();
    $query_langcode = $request->query->get($parameter_name);
    if (!isset($query_langcode) || !is_string($query_langcode)) {
      return NULL;
    }

    if (!isset($languages)) {
      $languages = $this->languageManager->getLanguages();
    }

    // Use the incoming language parameter value to map any possible parameter
    // overrides to their internal Drupal language id.
    $matched_langcode = $this->getLangcodeQueryParameterOverride($query_langcode);
    if (is_null($matched_langcode)) {
      return NULL;
    }

    $language = $languages[$matched_langcode] ?? NULL;
    if (!isset($language)) {
      return NULL;
    }

    return $language;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound(
    $path,
    &$options = [],
    Request $request = NULL,
    BubbleableMetadata $bubbleable_metadata = NULL
  ) {
    $languages = $this->languageManager->getLanguages();
    $parameter_name = $this->getLangcodeQueryParameterName();

    // Always prefer the explicitly specified language for the URL.
    $language = $options['language'] ?? NULL;

    // Otherwise, only add language to the URL if it's specified on the request
    // query, just to keep the language selected by user. Solution is far from
    // perfect, as you're trapped in a translation language, as the URL query
    // parameter is added to all the URLs. But without it all the URLs called by
    // the translation form may appear in default/user language and that's not
    // what we want.
    if (!isset($language) && isset($request)) {
      $language = $this->getRequestQueryLanguage($request, $languages);

      if ($bubbleable_metadata) {
        $bubbleable_metadata->addCacheContexts(['url.query_args:' . $parameter_name]);
      }
    }

    // Make sure we found a language and it's an enabled one.
    if (!isset($language) || !isset($languages[$language->getId()])) {
      return $path;
    }

    $options['language'] = $language;
    $options['query'][$parameter_name] = $language->getId();
    return $path;
  }

}
